package com.eya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * TODO(这个类的作用)
 *
 * @auther: luolin
 * @since: 2018/10/17 10:57
 */
@SpringBootApplication
public class SpringBootStarter {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootStarter.class);
	}


}
