package com.eya.config;

import com.eya.properties.HttpProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.undertow.UndertowEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.undertow.Undertow;

/**
 * undertow服务器配置
 *
 * @auther: luolin
 * @since: 2018/10/25 10:57
 */
@Configuration
//@Slf4j
@EnableConfigurationProperties(HttpProperties.class)
public class UndertowConfig {

	@Autowired
	private HttpProperties httpProperties;

	/**
	 * 配置undertow服务器监听http.port端口，实现SSL+普通http请求双端口响应
	 * @return
	 */
	@Bean
	public UndertowEmbeddedServletContainerFactory embeddedServletContainerFactory() {
		UndertowEmbeddedServletContainerFactory undertow = new UndertowEmbeddedServletContainerFactory();
		undertow.addBuilderCustomizers((Undertow.Builder builder) -> {
			builder.addHttpListener(httpProperties.getPort(), "0.0.0.0");
		});
		// 使用@Slf4j或@Log4j注解，在编译之后，会为类提供一个 属性名为log 的 log4j 日志对像
		// log.info("\n*** Undertow http setting successful." + httpProperties.getPort());
		return undertow;
	}

}
