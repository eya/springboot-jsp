package com.eya.config;

import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.undertow.servlet.api.SecurityConstraint;

/**
 * Tomcat的配置
 *
 * @auther: luolin
 * @since: 2018/10/25 11:03
 */
@Configuration
public class TomcatConfig {

	/**
	 * it's for set http url auto change to https
	 * @return
	 */
	//@Bean
	//public EmbeddedServletContainerFactory servletContainer(){
	//	TomcatEmbeddedServletContainerFactory tomcat=new TomcatEmbeddedServletContainerFactory(){
	//		@Override
	//		protected void postProcessContext(Context context) {
	//			SecurityConstraint securityConstraint=new SecurityConstraint();
	//			securityConstraint.setUserConstraint("CONFIDENTIAL");//confidential
	//			SecurityCollection collection=new SecurityCollection();
	//			collection.addPattern("/*");
	//			securityConstraint.addCollection(collection);
	//			context.addConstraint(securityConstraint);
	//		}
	//	};
	//	tomcat.addAdditionalTomcatConnectors(httpConnector());
	//	return tomcat;
	//}
	//
	//@Bean
	//public Connector httpConnector(){
	//	Connector connector=new Connector("org.apache.coyote.http11.Http11NioProtocol");
	//	connector.setScheme("http");
	//	connector.setPort(8080);
	//	connector.setSecure(false);
	//	connector.setRedirectPort(8443);
	//	return connector;
	//}

}
