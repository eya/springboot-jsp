package com.eya.controller;

import com.eya.rabbitmq.message.MessageContent;
import com.eya.rabbitmq.provider.MessageProvider;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Rabbit MQ 测试使用的控制器
 *
 * @auther: luolin
 * @since: 2018/10/25 15:48
 */
@RestController
@RequestMapping("rabbitMq")
@Api(description = "Rabbit MQ 测试使用的控制器")
public class RabbitMqTestController {

	@Resource
	private MessageProvider messageProvider;

	@RequestMapping("sendMessage")
	@ApiOperation(value = "发送RabbitMq测试消息 - 字符串", httpMethod = "GET")
	public String sendMessage() {
		messageProvider.send_2("Hello RabbitMq.");
		return "Message has been sent.";
	}

	@RequestMapping("sendObjectMessage")
	@ApiOperation(value = "发送RabbitMq测试消息 - 对象消息", httpMethod = "GET")
	public String sendObjectMessage() {
		messageProvider.send_1(new MessageContent(1, "发送RabbitMq测试消息 - 对象消息", "Derek"));
		return "Message has been sent.";
	}

}
