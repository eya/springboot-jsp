package com.eya.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 用户控制器
 *
 * @auther: luolin
 * @since: 2018/10/17 10:59
 */
@RestController
@RequestMapping("user")
@Api(description = "用户控制器")
public class UserController {

	@ApiOperation(value = "获取用户姓名",notes = "用于测试，无参数，固定返回",httpMethod = "GET")
	@RequestMapping("getUserName")
	public String getUserName(){
		return "Derek.";
	}

}
