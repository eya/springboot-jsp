package com.eya.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 使用lombok.Data注入application.yml 中的http下的属性
 *
 * @auther: luolin
 * @since: 2018/10/25 11:06
 */
@ConfigurationProperties(prefix = "http")
public class HttpProperties {

	private Integer port;

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
}
