package com.eya.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * 使用lombok.Data注入application.yml 中的server下的属性
 *
 * @auther: luolin
 * @since: 2018/10/25 10:58
 */
@ConfigurationProperties(prefix = "server")
public class ServerProperties {

	private Integer port;

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
}
