package com.eya.rabbitmq.config;


import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * RabbitMq配置
 * <p>参考：https://blog.csdn.net/hry2015/article/details/79545157</p>
 *
 * @auther: luolin
 * @since: 2018/10/25 15:42
 */
@Configuration
public class RabbitConfigure {

	// 队列名称
	public final static String SPRING_BOOT_QUEUE = "spring-boot-queue-2";
	// 交换机名称
	public final static String SPRING_BOOT_EXCHANGE = "spring-boot-exchange-2";
	// 绑定的值
	public static final String SPRING_BOOT_BIND_KEY = "spring-boot-bind-key-2";


	// === 在RabbitMQ上创建queue,exchange,binding 方法一：通过@Bean实现 begin ===
	/**
	 * 定义队列：
	 * @return
	 */
	@Bean
	Queue queue() {
		return new Queue(SPRING_BOOT_QUEUE, false);
	}

	/**
	 * 定义交换机
	 * @return
	 */
	//@Bean
	//TopicExchange exchange() {
	//	return new TopicExchange(SPRING_BOOT_EXCHANGE);
	//}
	//
	///**
	// * 定义绑定
	// * @param queue
	// * @param exchange
	// * @return
	// */
	//@Bean
	//Binding binding(Queue queue, TopicExchange exchange) {
	//	return BindingBuilder.bind(queue).to(exchange).with(SPRING_BOOT_BIND_KEY );
	//}

	/**
	 * 定义消息转换实例
	 * @return
	 */
	@Bean
	MessageConverter jackson2JsonMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}
}
