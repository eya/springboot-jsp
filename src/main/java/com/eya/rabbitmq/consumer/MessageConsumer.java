package com.eya.rabbitmq.consumer;

import com.eya.rabbitmq.config.RabbitConfigure;
import com.eya.rabbitmq.message.MessageContent;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消息消费者
 *
 * @auther: luolin
 * @since: 2018/10/25 15:46
 */
@Component
//@RabbitListener(queues = RabbitConfigure.SPRING_BOOT_QUEUE)
public class MessageConsumer {

	/**
	 * === 在RabbitMQ上创建queue,exchange,binding 方法二：直接在@RabbitListener声明 begin ===
	 * 接收
	 */
	//@RabbitListener(containerFactory = "rabbitListenerContainerFactory", bindings = @QueueBinding(value = @Queue(value =
	//		RabbitConfigure.SPRING_BOOT_QUEUE
	//		+ "3", durable = "true", autoDelete = "true"), exchange = @Exchange(value = RabbitConfigure.SPRING_BOOT_EXCHANGE, type = ExchangeTypes.TOPIC), key = RabbitConfigure.SPRING_BOOT_BIND_KEY))
	//public void receive_2(String content) {
	//	// ...
	//	System.out.println("[ReceiveMsg-2] receive msg: " + content);
	//}

	/**
	 * 获取信息:
	 * queue也可以支持RabbitMQ中对队列的模糊匹配
	 */
	//@RabbitHandler
	//@RabbitListener(queues = RabbitConfigure.SPRING_BOOT_QUEUE)
	public void receiveMsgContent1(MessageContent content) {
		System.out.println("[ReceiveMsgConvertMsg-MsgContent1] receive receiveMsgContent1 msg: " + content);
	}

}
