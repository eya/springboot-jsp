package com.eya.rabbitmq.provider;

import com.eya.rabbitmq.config.RabbitConfigure;
import com.eya.rabbitmq.message.MessageContent;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息生产者
 *
 * @auther: luolin
 * @since: 2018/10/25 15:45
 */
@Component
public class MessageProvider {

	// 此接口默认实现只有一个，且是RabbitAdmin，通过源码发现其内部实现实际是RabbitTemplate。所以AmqpAdmin和AmqpTemplate当前两者本质是相同的
	@Autowired
	private AmqpAdmin amqpAdmin;

	// 此接口的默认实现是RabbitTemplate，目前只有一个实现，
	@Autowired
	private AmqpTemplate amqpTemplate;

	/**
	 * 发送消息
	 */
	public void send_1(MessageContent content) {
		amqpTemplate.convertAndSend(RabbitConfigure.SPRING_BOOT_EXCHANGE, RabbitConfigure.SPRING_BOOT_BIND_KEY, content);
	}

	/**
	 * 发送消息
	 */
	public void send_2(String msgContent) {
		amqpTemplate.convertAndSend(RabbitConfigure.SPRING_BOOT_EXCHANGE, RabbitConfigure.SPRING_BOOT_BIND_KEY, msgContent);
	}

}
