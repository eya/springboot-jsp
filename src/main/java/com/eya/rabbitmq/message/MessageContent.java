package com.eya.rabbitmq.message;

import java.io.Serializable;

/**
 * TODO(这个类的作用)
 *
 * @auther: luolin
 * @since: 2018/10/26 15:12
 */
public class MessageContent implements Serializable {


	private Integer id;

	private String  remark;

	private String name;

	public MessageContent(Integer id, String remark, String name) {
		this.id = id;
		this.remark = remark;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
